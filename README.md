# Timbre Inteligente

Este proyecto permite a un **timbre normal** (digital o analogo) comportarse como un **timbre inteligente**, timbrando en dispositivos con **Alexa**.

## Requisitos

1. 2 **Cables de arduino** hembra hembra.
2. Modulo **Esp8266**.
3. Sensor de corriente **HW666**

## Instalacion

1. **Instalar el driver** del modulo Esp8266 a la computadora.
1. **Instalar Arduino Ide** para programar el modulo.
1. **Cargar librerias** al Arduino Ide.
1. **Descargar** codigo fuente de este repositorio.
1. **Modificar variables** segun la necesidad (datos de red, datos de cuenta, sensibilidad de la corriente, etc).
1. **Instalar** programa en Esp8266.
1. **Armar** circuito electrico.
1. **Configurar** Alexa para detectar el timbre.
1. **Probar**.
